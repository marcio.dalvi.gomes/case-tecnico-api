Para rodar o projeto corretamente, siga os passos a seguir:

1º Rode o script "SCRIPT.sql" em seu servidor MySql de preferência

Ps.: O nome da base tem que ser TesteTecnico

2º Vá em "CaseTecnico\AppSettings.json" e coloque os dados de conexao da sua base mysql na connection string "myConnection", troque os campos a seguir:

    -"{seuservidor}" = Coloque seu servidor 
    -"{usuario}" = Coloque o usuario de acesso ao banco 
    -"{Senha}" = Coloque a senha do banco

    
- PS: Lembre-se de retirar as chaves, sua connection string deve ficar assim:

        "myConnection": "Server=localhost;Port=3306;Database=TesteTecnico;Uid=root;Pwd=Teste*123;charset=utf8;"
    

3º Após configurar sua connection string abra a solution e execute o projeto "CaseTecnico.Api" na porta "localhost:5001".

 Provavelmente na hora que apertar F5 ja deve rodar nessa porta.




Link da API em um ambiente externo: 

https://casetecnico-api.azurewebsites.net/swagger/index.html