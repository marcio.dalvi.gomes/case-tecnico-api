﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Model
{
    public class ArvoreGrupoArvore
    {
        public int ArvoreGrupoArvoreId { get; set; }
        public int ArvoresId { get; set; }
        public int GrupoArvoreId { get; set; }
        public virtual Arvores Arvores { get; set; }
        public virtual GrupoArvore GrupoArvore { get; set; }
    }
}
