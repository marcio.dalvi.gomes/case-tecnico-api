﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Model
{
    public class Especies
    {
        public int EspeciesId { get; set; }
        public string Descricao { get; set; }
    }
}
