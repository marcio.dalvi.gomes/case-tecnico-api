﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Model
{
    public class GrupoArvore
    {
        public int GrupoArvoreId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
    }
}
