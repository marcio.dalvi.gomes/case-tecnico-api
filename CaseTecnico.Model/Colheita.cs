﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Model
{
    public class Colheita
    {
        public int ColheitaId { get; set; }
        public string Informacoes { get; set; }
        public DateTime DataDaColheita { get; set; }
        public float PesoBruto { get; set; }
        public int ArvoresId { get; set; }
        public virtual Arvores Arvores { get; set; }
    }
}
