﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Model
{
    public class Arvores
    {
        public int ArvoresId { get; set; }
        public string Descricao { get; set; }
        public int Idade { get; set; }
        public int EspeciesId { get; set; }
        public virtual Especies Especies { get; set; }
    }
}
