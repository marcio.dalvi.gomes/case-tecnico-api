﻿using CaseTecnico.Model;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Core
{
   public class Contexto : DbContext
    {
        public Contexto(DbContextOptions<Contexto> options) : base(options)
        {
        }
        public DbSet<Especies> Especies { get; set; }
        public DbSet<Arvores> Arvores { get; set; }
        public DbSet<GrupoArvore> GrupoArvore { get; set; }
        public DbSet<ArvoreGrupoArvore> ArvoreGrupoArvore { get; set; }
        public DbSet<Colheita> Colheita { get; set; }
    }
}
