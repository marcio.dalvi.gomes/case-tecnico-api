﻿using CaseTecnico.Api.Controllers;
using CaseTecnico.Service.Spec;
using System;
using System.Text;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Xunit;
using Newtonsoft.Json;
using CaseTecnico.Model;

namespace CaseTecnico.Api.Test
{
    public class ArvoreControllerTest
    {
        ArvoresController _controller;
        IArvoresService _arvoresService;
        public ArvoreControllerTest()
        {
            _arvoresService = new ArvoreServiceFake();
            _controller = new ArvoresController(_arvoresService);
        }
        [Fact]
        public void Post_WhenCalled_ReturnsBoolean()
        {
            // Act
            var okResult = _controller.Post(new Arvores() { Idade = 3, EspeciesId = 7, Descricao = "Oi marcio"});
            // Assert
            Assert.IsType<bool>(okResult.Value);
        }
        [Fact]
        public void Get_WhenCalled_ReturnsAllItems()
        {
            // Act
            var okResult = _controller.Get().Value;
            // Assert
            var items = Assert.IsType<List<Arvores>>(okResult);
            Assert.Equal(4, items.Count);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsItemById()
        {
            // Act
            var okResult = _controller.GetById(1).Value;
            // Assert
            Assert.IsType<Arvores>(okResult);
        }
        [Fact]
        public void Edit_WhenCalled_ReturnsItemEdited()
        {
            // Act
            var okResult = _controller.Edit(1,new Arvores() {Descricao = "Teste3", EspeciesId = 9, Idade = 23 }).Value;
            // Assert
            Assert.IsType<Arvores>(okResult);
        }
    }
}
