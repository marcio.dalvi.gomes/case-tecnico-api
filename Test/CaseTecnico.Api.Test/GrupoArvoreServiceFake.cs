﻿using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CaseTecnico.Api.Test
{
    public class GrupoArvoreServiceFake : IGrupoArvoreService
    {
        private readonly List<GrupoArvore> _grupoArvores;
        public GrupoArvoreServiceFake()
        {
            _grupoArvores = new List<GrupoArvore>()
            {
                new GrupoArvore()
                {
                    Descricao = "Arvore 1",
                    Nome = "Grupo 1",
                    GrupoArvoreId = 1
                },new GrupoArvore()
                {
                    Descricao = "Arvore 2",
                    Nome = "Grupo 2",
                    GrupoArvoreId = 2
                },new GrupoArvore()
                {
                    Descricao = "Arvore 3",
                    Nome = "Grupo 3",
                    GrupoArvoreId = 3
                },new GrupoArvore()
                {
                    Descricao = "Arvore 4",
                    Nome = "Grupo 4",
                    GrupoArvoreId = 4
                },new GrupoArvore()
                {
                    Descricao = "Arvore 5",
                    Nome = "Grupo 5",
                    GrupoArvoreId = 5
                },
            };
        }

        public bool Delete(int id)
        {
            var remove = _grupoArvores.First(x => x.GrupoArvoreId == id);
            return _grupoArvores.Remove(remove);
        }

        public GrupoArvore Edit(int id, GrupoArvore GrupoArvore)
        {
            var edit = _grupoArvores.First(x => x.GrupoArvoreId == id);
            _grupoArvores.Remove(edit);
            edit.GrupoArvoreId = id;
            edit.Nome = GrupoArvore.Nome;
            edit.Descricao = GrupoArvore.Descricao;
            _grupoArvores.Add(edit);
            return edit;
        }

        public List<GrupoArvore> GetAll()
        {
            return _grupoArvores;
        }

        public GrupoArvore GetById(int id)
        {
            return _grupoArvores.First(x => x.GrupoArvoreId == id);
        }

        public GrupoArvore Insert(GrupoArvore GrupoArvore)
        {
            _grupoArvores.Add(GrupoArvore);
            return _grupoArvores.First(x => x == GrupoArvore);
        }
    }
}
