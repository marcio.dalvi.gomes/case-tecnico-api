﻿using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CaseTecnico.Api.Test
{
    public class ArvoreServiceFake : IArvoresService
    {
        private readonly List<Arvores> _arvores;
        public ArvoreServiceFake()
        {
            _arvores = new List<Arvores>()
            {
                new Arvores()
                {
                    ArvoresId = 1,
                    Descricao = "Arvore 1",
                    EspeciesId = 1,
                    Idade = 25
                },
                new Arvores()
                {
                    ArvoresId = 2,
                    Descricao = "Arvore 2",
                    EspeciesId = 3,
                    Idade = 33
                },
                new Arvores()
                {
                    ArvoresId = 3,
                    Descricao = "Arvore 3",
                    EspeciesId = 2,
                    Idade = 1
                },
                new Arvores()
                {
                    ArvoresId = 4,
                    Descricao = "Arvore 4",
                    EspeciesId = 4,
                    Idade = 67
                }
            };
        }
        public bool Delete(int id)
        {
            var arvore = _arvores.First(x => x.ArvoresId == id);
            _arvores.Remove(arvore);
            return true;
        }

        public Arvores Edit(int id, Arvores arvores)
        {
            var arvore = _arvores.FirstOrDefault(x => x.ArvoresId == id);
            _arvores.Remove(arvore);
            arvore.ArvoresId = id;
            arvore.Descricao = arvores.Descricao;
            arvore.EspeciesId = arvores.EspeciesId;
            arvore.Idade = arvores.Idade;
            _arvores.Add(arvore);
            return arvore;
        }

        public List<Arvores> GetAll()
        {
            return _arvores;
        }

        public Arvores GetById(int id)
        {
            var arvore = _arvores.First(x => x.ArvoresId == id);
            return arvore;
        }

        public bool Insert(Arvores arvores)
        {
            _arvores.Add(arvores);
            return true;
        }
    }
}
