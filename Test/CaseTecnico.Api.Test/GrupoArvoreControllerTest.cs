﻿using CaseTecnico.Api.Controllers;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CaseTecnico.Api.Test
{
    public class GrupoArvoreControllerTest
    {

        GrupoArvoreController _controller;
        IGrupoArvoreService _service;
        public GrupoArvoreControllerTest()
        {
            _service = new GrupoArvoreServiceFake();
            _controller = new GrupoArvoreController(_service);
        }

        [Fact]
        public void Post_WhenCalled_ReturnsBoolean()
        {
            // Act
            var okResult = _controller.Post(new GrupoArvore()
            {
                Descricao = "TEST xUnit",
                GrupoArvoreId = 30,
                Nome = "TESTE xUnit"
            });
            // Assert
            Assert.IsType<GrupoArvore>(okResult.Value);
        }
        [Fact]
        public void Get_WhenCalled_ReturnsAllItems()
        {
            // Act
            var okResult = _controller.Get().Value;
            // Assert
            var items = Assert.IsType<List<GrupoArvore>>(okResult);
            Assert.Equal(5, items.Count);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsItemById()
        {
            // Act
            var okResult = _controller.GetById(1).Value;
            // Assert
            Assert.IsType<GrupoArvore>(okResult);
        }
        [Fact]
        public void Edit_WhenCalled_ReturnsItemEdited()
        {
            // Act
            var okResult = _controller.Edit(1, new GrupoArvore() {Nome = "GRUPO ARVORE EDITADO", Descricao = "Nova descrição" }).Value;
            // Assert
            Assert.IsType<GrupoArvore>(okResult);
        }
    }
}
