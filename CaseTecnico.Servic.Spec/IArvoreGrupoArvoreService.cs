﻿using CaseTecnico.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Service.Spec
{
    public interface IArvoreGrupoArvoreService
    {

        List<ArvoreGrupoArvore> GetAll();
        bool Insert(ArvoreGrupoArvore ArvoreGrupoArvore);
        bool InsertMultiples(List<ArvoreGrupoArvore> arvoreGrupoArvores);
        ArvoreGrupoArvore GetById(int id);
        List<ArvoreGrupoArvore> GetByGrupoArvore(int id);
        ArvoreGrupoArvore Edit(int id, ArvoreGrupoArvore ArvoreGrupoArvore);
        bool EditMultiples(List<ArvoreGrupoArvore> arvoreGrupoArvores);
        bool Delete(int id);
        bool DeleteMultiples(int[] ids);
    }
}
