﻿using CaseTecnico.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Service.Spec
{
    public interface IGrupoArvoreService
    {
        List<GrupoArvore> GetAll();
        GrupoArvore Insert(GrupoArvore GrupoArvore);
        GrupoArvore GetById(int id);
        GrupoArvore Edit(int id, GrupoArvore GrupoArvore);
        bool Delete(int id);
    }
}
