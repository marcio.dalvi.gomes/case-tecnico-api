﻿using CaseTecnico.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Service.Spec
{
    public interface IColheitaService
    {
        List<Colheita> GetAll();
        bool Insert(Colheita colheita);
        Colheita GetById(int id);
        Colheita Edit(int id, Colheita colheita);
        bool Delete(int id);
    }
}
