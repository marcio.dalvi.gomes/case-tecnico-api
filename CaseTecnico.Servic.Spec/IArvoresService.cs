﻿using CaseTecnico.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Service.Spec
{
    public interface IArvoresService
    {
        List<Arvores> GetAll();
        bool Insert(Arvores arvores);
        Arvores GetById(int id);
        Arvores Edit(int id, Arvores especies);
        bool Delete(int id);
    }
}
