﻿using CaseTecnico.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnico.Service.Spec
{
    public interface IEspeciesService
    {
        List<Especies> GetAll();
        bool Insert(Especies especies);
        Especies GetById(int id);
        Especies Edit(int id,Especies especies);
        bool Delete(int id);
    }
}
