﻿using CaseTecnico.Core;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace CaseTecnico.Service
{
    public class ColheitaService : IColheitaService
    {
        private readonly Contexto _context;

        public ColheitaService(Contexto context)
        {
            this._context = context;
        }

        public Colheita Edit(int id, Colheita colheita)
        {
            var colheitaValues = _context.Colheita
                .Include(x => x.Arvores)
                .Include(x => x.Arvores.Especies)
                .FirstOrDefault(x => x.ColheitaId == id);
            if (colheitaValues == null)
                return null;


            if (!string.IsNullOrEmpty(colheita.Informacoes))
                colheitaValues.Informacoes = colheita.Informacoes;
            if (colheita.DataDaColheita != null)
                colheitaValues.DataDaColheita = colheita.DataDaColheita;
            if (colheita.PesoBruto > 0.0)
                colheitaValues.PesoBruto = colheita.PesoBruto;
            if (colheita.ArvoresId > 0)
                colheitaValues.ArvoresId = colheita.ArvoresId;

            _context.Colheita.Update(colheitaValues);
            _context.SaveChanges();
            return colheitaValues;
        }

        public List<Colheita> GetAll()
        {
            return _context.Colheita
                .Include(x => x.Arvores)
                .Include(x => x.Arvores.Especies)
                .ToList();
        }

        public Colheita GetById(int id)
        {
            var result = _context.Colheita
                .Include(x => x.Arvores)
                .Include(x => x.Arvores.Especies)
                .FirstOrDefault(x => x.ColheitaId == id);
            return result;
        }

        public bool Insert(Colheita colheita)
        {
            if (string.IsNullOrEmpty(colheita.Informacoes) || colheita.DataDaColheita == null || colheita.ArvoresId <= 0 || colheita.PesoBruto <= 0.0)
            {
                return false;
            }
            _context.Colheita.Add(colheita);
            _context.SaveChanges();

            return true;
        }

        public bool Delete(int id)
        {
            var colheita = _context.Colheita.FirstOrDefault(x => x.ColheitaId == id);
            if (colheita == null)
                return false;

            _context.Colheita.Remove(colheita);
            _context.SaveChanges();
            return true;
        }
    }
}
