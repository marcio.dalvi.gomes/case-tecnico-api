﻿using CaseTecnico.Core;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseTecnico.Service
{
    public class EspeciesService : IEspeciesService
    {
        private readonly Contexto _context;

        public EspeciesService(Contexto context)
        {
            this._context = context;
        }

        public Especies Edit(int id, Especies especies)
        {
            var especievalue = _context.Especies.FirstOrDefault(x => x.EspeciesId == id);
            if (especievalue == null)
                return null;

            especievalue.Descricao = especies.Descricao;

            _context.Especies.Update(especievalue);
            _context.SaveChanges();
            return especievalue;
        }

        public List<Especies> GetAll()
        {
            return _context.Especies.ToList();
        }

        public Especies GetById(int id)
        {
            var result = _context.Especies.FirstOrDefault(x => x.EspeciesId == id);
            return result;
        }

        public bool Insert(Especies especies)
        {
            if (string.IsNullOrEmpty(especies.Descricao))
            {
                return false;
            }
            _context.Especies.Add(especies);
            _context.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var especies = _context.Especies.FirstOrDefault(x => x.EspeciesId == id);
            if (especies == null)
                return false;

            _context.Especies.Remove(especies);
            _context.SaveChanges();
            return true;
        }
    }
}
