﻿using CaseTecnico.Core;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace CaseTecnico.Service
{
    public class ArvoreGrupoArvoreService : IArvoreGrupoArvoreService
    {
        private readonly Contexto _context;

        public ArvoreGrupoArvoreService(Contexto context)
        {
            this._context = context;
        }

        public ArvoreGrupoArvore Edit(int id, ArvoreGrupoArvore arvoreGrupoArvore)
        {
            var arvoreGrupoArvoreValue = _context.ArvoreGrupoArvore.Include(x => x.Arvores).Include(x => x.GrupoArvore)
                .FirstOrDefault(x => x.ArvoreGrupoArvoreId == id);
            if (arvoreGrupoArvoreValue == null)
                return null;
            arvoreGrupoArvoreValue.ArvoresId = arvoreGrupoArvore.ArvoresId;
            arvoreGrupoArvoreValue.GrupoArvoreId = arvoreGrupoArvore.GrupoArvoreId;
            _context.ArvoreGrupoArvore.Update(arvoreGrupoArvore);
            _context.SaveChanges();
            return arvoreGrupoArvoreValue;
        }
        public bool EditMultiples(List<ArvoreGrupoArvore> arvoreGrupoArvores)
        {
            try
            {

                foreach (var item in arvoreGrupoArvores)
                {
                    var arvoreGrupoArvoreValue = _context.ArvoreGrupoArvore.Include(x => x.Arvores).Include(x => x.GrupoArvore)
                        .FirstOrDefault(x => x.ArvoreGrupoArvoreId == item.ArvoreGrupoArvoreId);
                    if (arvoreGrupoArvoreValue == null)
                        return false;
                    arvoreGrupoArvoreValue.ArvoresId = item.ArvoresId;
                    arvoreGrupoArvoreValue.GrupoArvoreId = item.GrupoArvoreId;
                    _context.ArvoreGrupoArvore.Update(arvoreGrupoArvoreValue);

                }
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<ArvoreGrupoArvore> GetAll()
        {
            return _context.ArvoreGrupoArvore.Include(x => x.Arvores).Include(x => x.GrupoArvore).ToList();
        }

        public ArvoreGrupoArvore GetById(int id)
        {
            var result = _context.ArvoreGrupoArvore.Include(x => x.Arvores).Include(x => x.GrupoArvore).FirstOrDefault(x => x.ArvoreGrupoArvoreId == id);
            return result;
        }
        public List<ArvoreGrupoArvore> GetByGrupoArvore(int id)
        {
            var result = _context.ArvoreGrupoArvore.Include(x => x.Arvores).Include(x => x.GrupoArvore).Where(x => x.GrupoArvoreId == id).ToList();
            return result;
        }

        public bool Insert(ArvoreGrupoArvore arvoreGrupoArvore)
        {
            _context.ArvoreGrupoArvore.Add(arvoreGrupoArvore);
            _context.SaveChanges();

            return true;
        }
        public bool InsertMultiples(List<ArvoreGrupoArvore> arvoreGrupoArvore)
        {
            foreach (var item in arvoreGrupoArvore)
            {
                _context.ArvoreGrupoArvore.Add(item);
            }
            _context.SaveChanges();

            return true;
        }

        public bool Delete(int id)
        {
            var arvoreGrupoArvore = _context.ArvoreGrupoArvore.FirstOrDefault(x => x.ArvoreGrupoArvoreId == id);
            if (arvoreGrupoArvore == null)
                return false;

            _context.ArvoreGrupoArvore.Remove(arvoreGrupoArvore);
            _context.SaveChanges();
            return true;
        }
        public bool DeleteMultiples(int[] ids)
        {
            foreach (var id in ids)
            {

                var arvoreGrupoArvore = _context.ArvoreGrupoArvore.FirstOrDefault(x => x.ArvoreGrupoArvoreId == id);
                if (arvoreGrupoArvore == null)
                    return false;

                _context.ArvoreGrupoArvore.Remove(arvoreGrupoArvore);

            }
            _context.SaveChanges();
            return true;
        }
    }
}
