﻿using CaseTecnico.Core;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace CaseTecnico.Service
{
    public class ArvoresService : IArvoresService
    {
        private readonly Contexto _context;

        public ArvoresService(Contexto context)
        {
            this._context = context;
        }

        public Arvores Edit(int id, Arvores arvores)
        {
            var arvoresValue = _context.Arvores
                .Include(x => x.Especies)
                .FirstOrDefault(x => x.ArvoresId == id);
            if (arvoresValue == null)
                return null;

            arvoresValue.Descricao = arvores.Descricao;
            arvoresValue.Idade = arvores.Idade;
            arvoresValue.EspeciesId = arvores.EspeciesId;

            _context.Arvores.Update(arvoresValue);
            _context.SaveChanges();
            return arvoresValue;
        }

        public List<Arvores> GetAll()
        {
            return _context.Arvores
                .Include(x => x.Especies)
                .ToList();
        }

        public Arvores GetById(int id)
        {
            var result = _context.Arvores
                .Include(x => x.Especies)
                .FirstOrDefault(x => x.ArvoresId == id);
            return result;
        }

        public bool Insert(Arvores arvores)
        {
            try
            {

                if (string.IsNullOrEmpty(arvores.Descricao) || arvores.Idade == null || arvores.EspeciesId <= 0)
                {
                    return false;
                }
                _context.Arvores.Add(new Arvores() { Descricao = arvores.Descricao, EspeciesId = arvores.EspeciesId, Idade = arvores.Idade });
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            var arvore = _context.Arvores.FirstOrDefault(x => x.ArvoresId == id);
            if (arvore == null)
                return false;

            _context.Arvores.Remove(arvore);
            _context.SaveChanges();
            return true;
        }
    }
}
