﻿using CaseTecnico.Core;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace CaseTecnico.Service
{
    public class GrupoArvoreService : IGrupoArvoreService
    {
        private readonly Contexto _context;

        public GrupoArvoreService(Contexto context)
        {
            this._context = context;
        }

        public GrupoArvore Edit(int id, GrupoArvore GrupoArvore)
        {
            var GrupoArvoreValue = _context.GrupoArvore
                .FirstOrDefault(x => x.GrupoArvoreId == id);
            if (GrupoArvoreValue == null)
                return null;

            if (!string.IsNullOrEmpty(GrupoArvore.Descricao))
                GrupoArvoreValue.Descricao = GrupoArvore.Descricao;
            if (!string.IsNullOrEmpty(GrupoArvore.Nome))
                GrupoArvoreValue.Nome = GrupoArvore.Nome;

            _context.GrupoArvore.Update(GrupoArvoreValue);
            _context.SaveChanges();
            return GrupoArvoreValue;
        }

        public List<GrupoArvore> GetAll()
        {
            return _context.GrupoArvore.ToList();
        }

        public GrupoArvore GetById(int id)
        {
            var result = _context.GrupoArvore.FirstOrDefault(x => x.GrupoArvoreId == id);
            return result;
        }

        public GrupoArvore Insert(GrupoArvore grupoArvore)
        {
            if (string.IsNullOrEmpty(grupoArvore.Descricao) || string.IsNullOrEmpty(grupoArvore.Nome))
            {
                return null;
            }
            _context.GrupoArvore.Add(grupoArvore);
            _context.SaveChanges();

            return grupoArvore;
        }

        public bool Delete(int id)
        {
            var grupoArvore = _context.GrupoArvore.FirstOrDefault(x => x.GrupoArvoreId == id);
            if (grupoArvore == null)
                return false;

            _context.GrupoArvore.Remove(grupoArvore);
            _context.SaveChanges();
            return true;
        }
    }
}
