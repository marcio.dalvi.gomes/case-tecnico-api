﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CaseTecnico.Api.Controllers
{
    [ApiController, Route("[controller]")]
    public class EspeciesController : ControllerBase
    {
        private readonly IEspeciesService _especiesService;

        public EspeciesController(IEspeciesService especiesService)
        {
            _especiesService = especiesService;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var especies = _especiesService.GetAll();
            return new JsonResult(especies);
        }
        [HttpPost]
        public JsonResult Post([FromBody]Especies especies)
        {
            var result = _especiesService.Insert(especies);
            return new JsonResult(result);
        }

        [HttpGet("{id}")]
        public JsonResult GetById([FromRoute] int id)
        {
            var result = _especiesService.GetById(id);
            return new JsonResult(result);
        }
        [HttpPut("{id}")]
        public JsonResult Edit([FromRoute] int id,[FromBody]Especies especies)
        {
            var result = _especiesService.Edit(id,especies);
            return new JsonResult(result);
        }
        [HttpDelete("{id}")]
        public JsonResult Delete([FromRoute] int id)
        {
            var result = _especiesService.Delete(id);
            return new JsonResult(result);
        }
    }
}
