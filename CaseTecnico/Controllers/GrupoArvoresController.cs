﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CaseTecnico.Api.Controllers
{
    [ApiController, Route("[controller]")]
    public class GrupoArvoreController : ControllerBase
    {
        private readonly IGrupoArvoreService _grupoArvoreService;

        public GrupoArvoreController(IGrupoArvoreService GrupoArvoreervice)
        {
            _grupoArvoreService = GrupoArvoreervice;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var result = _grupoArvoreService.GetAll();
            return new JsonResult(result);
        }
        [HttpPost]
        public JsonResult Post([FromBody] GrupoArvore GrupoArvore)
        {
            var result = _grupoArvoreService.Insert(GrupoArvore);
            return new JsonResult(result);
        }

        [HttpGet("{id}")]
        public JsonResult GetById([FromRoute] int id)
        {
            var result = _grupoArvoreService.GetById(id);
            return new JsonResult(result);
        }
        [HttpPut("{id}")]
        public JsonResult Edit([FromRoute] int id, [FromBody] GrupoArvore GrupoArvore)
        {
            var result = _grupoArvoreService.Edit(id, GrupoArvore);
            return new JsonResult(result);
        }
        [HttpDelete("{id}")]
        public JsonResult Delete([FromRoute] int id)
        {
            var result = _grupoArvoreService.Delete(id);
            return new JsonResult(result);
        }
    }
}
