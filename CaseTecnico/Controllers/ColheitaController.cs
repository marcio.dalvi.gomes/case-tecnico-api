﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CaseTecnico.Api.Controllers
{
    [ApiController, Route("[controller]")]
    public class ColheitaController : ControllerBase
    {
        private readonly IColheitaService _colheitaService;

        public ColheitaController(IColheitaService colheitaService)
        {
            _colheitaService = colheitaService;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var arvores = _colheitaService.GetAll();
            return new JsonResult(arvores);
        }
        [HttpPost]
        public JsonResult Post([FromBody] Colheita colheita)
        {
            var result = _colheitaService.Insert(colheita);
            return new JsonResult(result);
        }

        [HttpGet("{id}")]
        public JsonResult GetById([FromRoute] int id)
        {
            var result = _colheitaService.GetById(id);
            return new JsonResult(result);
        }
        [HttpPut("{id}")]
        public JsonResult Edit([FromRoute] int id, [FromBody] Colheita colheita)
        {
            var result = _colheitaService.Edit(id, colheita);
            return new JsonResult(result);
        }
        [HttpDelete("{id}")]
        public JsonResult Delete([FromRoute] int id)
        {
            var result = _colheitaService.Delete(id);
            return new JsonResult(result);
        }
    }
}
