﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CaseTecnico.Api.Controllers
{
    [ApiController, Route("[controller]")]
    public class ArvoreGrupoArvoreController : ControllerBase
    {
        private readonly IArvoreGrupoArvoreService _arvoreGrupoArvoreService;

        public ArvoreGrupoArvoreController(IArvoreGrupoArvoreService arvoreGrupoArvore)
        {
            _arvoreGrupoArvoreService = arvoreGrupoArvore;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var result = _arvoreGrupoArvoreService.GetAll();
            return new JsonResult(result);
        }
        [HttpPost]
        public JsonResult Post([FromBody] ArvoreGrupoArvore arvoreGrupoArvore)
        {
            var result = _arvoreGrupoArvoreService.Insert(arvoreGrupoArvore);
            return new JsonResult(result);
        }
        [HttpPost("Multiplos")]
        public JsonResult Post([FromBody] List<ArvoreGrupoArvore> arvoreGrupoArvore)
        {
            var result = _arvoreGrupoArvoreService.InsertMultiples(arvoreGrupoArvore);
            return new JsonResult(result);
        }

        [HttpGet("{id}")]
        public JsonResult GetById([FromRoute] int id)
        {
            var result = _arvoreGrupoArvoreService.GetById(id);
            return new JsonResult(result);
        }
        [HttpGet("{id}/GrupoArvore")]
        public JsonResult GetByGrupoArvore([FromRoute] int id)
        {
            var result = _arvoreGrupoArvoreService.GetByGrupoArvore(id);
            return new JsonResult(result);
        }
        [HttpPut("{id}")]
        public JsonResult Edit([FromRoute] int id, [FromBody] ArvoreGrupoArvore arvoreGrupoArvore)
        {
            var result = _arvoreGrupoArvoreService.Edit(id, arvoreGrupoArvore);
            return new JsonResult(result);
        }
        [HttpPut("Multiplos")]
        public JsonResult Edit([FromBody] List<ArvoreGrupoArvore> arvoreGrupoArvore)
        {
            var result = _arvoreGrupoArvoreService.EditMultiples(arvoreGrupoArvore);
            return new JsonResult(result);
        }
        [HttpDelete("{id}")]
        public JsonResult Delete([FromRoute] int id)
        {
            var result = _arvoreGrupoArvoreService.Delete(id);
            return new JsonResult(result);
        }
        [HttpDelete]
        public JsonResult Delete([FromBody] int[] ids)
        {
            var result = _arvoreGrupoArvoreService.DeleteMultiples(ids);
            return new JsonResult(result);
        }
    }
}
