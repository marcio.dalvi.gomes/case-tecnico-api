﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaseTecnico.Model;
using CaseTecnico.Service.Spec;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CaseTecnico.Api.Controllers
{
    [ApiController, Route("[controller]")]
    public class ArvoresController : ControllerBase
    {
        private readonly IArvoresService _arvoresService;

        public ArvoresController(IArvoresService arvoresService)
        {
            _arvoresService = arvoresService;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var arvores = _arvoresService.GetAll();
            return new JsonResult(arvores);
        }
        [HttpPost]
        public JsonResult Post([FromBody] Arvores arvore)
        {
            var result = _arvoresService.Insert(arvore);
            return new JsonResult(result);
        }

        [HttpGet("{id}")]
        public JsonResult GetById([FromRoute] int id)
        {
            var result = _arvoresService.GetById(id);
            return new JsonResult(result);
        }
        [HttpPut("{id}")]
        public JsonResult Edit([FromRoute] int id, [FromBody] Arvores arvore)
        {
            var result = _arvoresService.Edit(id, arvore);
            return new JsonResult(result);
        }
        [HttpDelete("{id}")]
        public JsonResult Delete([FromRoute] int id)
        {
            var result = _arvoresService.Delete(id);
            return new JsonResult(result);
        }
    }
}
